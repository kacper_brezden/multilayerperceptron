package net

/**
 * Created by rebzden on 05/11/16.
 */
class HiddenLayer(var size: Int, var weightsAmount: Int) : Layer(size, weightsAmount) {

    override fun toString(): String {
        return "net.HiddenLayer(neuronCount=$neuronCount, inputs=${this.layerInputs().size} outputs=${this.layerOutputs().size})"
    }
}