package net

/**
 * Created by rebzden on 17/11/16.
 */
class SigmoidFunction : ActivationFunction {

    override fun evaluate(value: Double): Double {
        return 1 / (1 + Math.exp(-value))
    }

    override fun evaluateDerivate(value: Double): Double {
        return value - Math.pow(value, 2.0)
    }

}