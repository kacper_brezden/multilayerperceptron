package net

import java.util.*

/**
 * Created by rebzden on 05/11/16.
 */
class Neuron() {

    var inputs: ArrayList<Double> = arrayListOf()
    var weights: ArrayList<Double> = arrayListOf()
    var output: Double = 0.0
    var bias: Double = 0.0
    var delta: Double = 0.0
    var net: Double = 0.0

    init {

    }

    fun countOutput(activationFunction: ActivationFunction) {
        this.net = this.countNet()
        this.output = activationFunction.evaluate(this.net)
    }

    fun countNet(): Double {
        var net = this.inputs.indices.sumByDouble {
            this.inputs[it] * this.weights[it]
        }
        net += this.bias
        return net
    }

    fun countError(): Double {
        var error = this.weights.indices.sumByDouble {
            this.delta * this.weights[it]
        }
        return error
    }

    fun countError(delta: Double): Double {
        var error = this.weights.indices.sumByDouble {
            delta * this.weights[it]
        }
        return error
    }

    fun updateWeights(learningRate: Double) {
        for (i in this.weights.indices) {
            var change = learningRate * this.delta * inputs[i]
            this.weights[i] += change
        }
        this.bias += learningRate * this.delta
    }
}