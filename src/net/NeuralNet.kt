package net

import java.io.File
import java.util.*
import javax.imageio.ImageIO

/**
 * Created by rebzden on 05/11/16.
 */
class NeuralNet(var trainingPath: String, var testPath: String, var hiddenLayerSize: Int, var hiddenLayerCount: Int,
                var outputLayerSize: Int, var learningRate: Double, var activationFunction: ActivationFunction,
                var epochNumber: Int, var samplesInEpoch: Int, var sparsityParam: Double, val sparsityWeight: Double
) {

    var learningSet: ArrayList<ArrayList<Double>> = arrayListOf()
    var inputLayerSize: Int = 0
    var inputWidth: Double = .0
    var inputHeight: Double = .0
    var layers: ArrayList<Layer> = arrayListOf()
    var expectedValue: ArrayList<Double> = arrayListOf()
    var errors: ArrayList<Double> = arrayListOf()
    var epochErrors: ArrayList<Double> = arrayListOf()
    var avgHiddenActivation: ArrayList<Double> = arrayListOf()

    init {
        readFiles(this.trainingPath, true)
        createLayers()
        countAvgActivation()
        for (i in 0..epochNumber) {
            learn()
        }
    }

    private fun countAvgActivation() {
        for (i in 0..samplesInEpoch) {
            populateInput()
            propagate()
            this.layers[1].neurons.forEachIndexed { i, neuron ->
                val oldAvg = this.avgHiddenActivation.getOrElse(i, { value -> 0.0 }) + neuron.net
                this.avgHiddenActivation.add(i, oldAvg)
            }
        }
        for (i in 0..this.avgHiddenActivation.size - 1) {
            this.avgHiddenActivation[i] = this.avgHiddenActivation[i] / samplesInEpoch
        }
    }

    private fun printLayers() {
        this.layers.forEach(::println)
    }

    fun readFiles(path: String, noise: Boolean) {
        var dataset = File(path)
        var width = 0
        var height = 0
        var imageProperties: ArrayList<Double>
        var rand = Random()
        for (dataFile in dataset.listFiles()) {
            var dataImage = ImageIO.read(dataFile)
            width = dataImage.width
            height = dataImage.height
            imageProperties = arrayListOf()
            for (x in 0..dataImage.width - 1) {
                for (y in 0..dataImage.height - 1) {
                    val clr = dataImage.getRGB(x, y)
                    val red = clr and 0x00ff0000 shr 16
                    val green = clr and 0x0000ff00 shr 8
                    val blue = clr and 0x000000ff
                    val colorSum = red + green + blue
                    val normalizedColor = (1.0 / 765.0) * colorSum
                    imageProperties.add(normalizedColor)
                }
            }
            this.learningSet.add(imageProperties)
        }
        this.inputWidth = width.toDouble()
        this.inputHeight = height.toDouble()
        this.inputLayerSize = width * height
    }

    fun createLayers() {
        this.layers.add(InputLayer(this.inputLayerSize))
        for (i in 1..this.hiddenLayerCount) {
            this.layers.add(HiddenLayer(this.hiddenLayerSize, this.layers[i - 1].neuronCount))
        }
        this.layers.add(OutputLayer(this.outputLayerSize, this.layers.last().neuronCount))
    }

    fun learn() {
        for (i in 0..samplesInEpoch) {
            populateInput()
            propagate()
            backPropagate()
        }
        var epochError = this.epochErrors.indices.sumByDouble {
            Math.abs(this.epochErrors[it])
        } / this.epochErrors.size
        this.errors.add(epochError)
        this.epochErrors = arrayListOf()
    }

    fun populateInput() {
        var rand = Random()
        val entryValues = this.learningSet[rand.nextInt(this.learningSet.size - 1)]
        this.expectedValue = entryValues
        this.layers[0].neurons.forEachIndexed { i, neuron ->
            neuron.output = entryValues[i]
        }
    }

    fun populateInput(dataset: ArrayList<Double>) {
        this.layers[0].neurons.forEachIndexed { i, neuron ->
            neuron.output = dataset[i]
        }
    }

    private fun propagate() {
        //Loop through all layers
        for (i in 1..this.layers.size - 1) {
            val previousLayer = this.layers[i - 1]
            //Loop through all neurons of current layer
            this.layers[i].neurons.forEach { neuron ->
                //Pass previous layer outputs as current layer inputs
                neuron.inputs = previousLayer.layerOutputs()
                neuron.countOutput(this.activationFunction)
            }
        }
    }

    private fun backPropagate() {
        countOutputError()
        countHiddenError()
        adjustWeights()
    }

    fun countOutputError() {
        var outputError = 0.0
        this.layers.last().neurons.forEachIndexed { i, neuron ->
            val error = Math.pow(this.expectedValue[i] - neuron.output, 2.0) / 2
            outputError += error
            neuron.delta = error * this.activationFunction.evaluateDerivate(neuron.output)
        }
        this.epochErrors.add(outputError)
    }

    fun countHiddenError() {

        for (i in (this.layers.size - 2) downTo 0) {
            val lastLayer = this.layers[i + 1]
            this.layers[i].neurons.forEachIndexed { i, neuron ->
                var error = 0.0
                lastLayer.neurons.forEach { lastNeuron ->
                    error += neuron.countError(lastNeuron.delta)
                }
                var sparsinity = 0.0
                if (this.avgHiddenActivation.getOrNull(i) != null && i ==1) {
                    sparsinity = this.sparsityWeight * ((-this.sparsityParam / this.avgHiddenActivation[i]) + (1 - this.sparsityParam / 1 - avgHiddenActivation[i]))
                }

                neuron.delta = (error + sparsinity) * this.activationFunction.evaluateDerivate(neuron.output)
            }
        }
    }

    fun adjustWeights() {
        for (i in this.layers.indices) {
            this.layers[i].neurons.forEach { neuron ->
                neuron.updateWeights(this.learningRate)
            }
        }
    }

    fun check() {
        var success: Int = 0
        this.learningSet = arrayListOf()
        readFiles(this.testPath, false)
        for (i in 0..this.learningSet.size - 1) {
            val testPhoto = this.learningSet[i]
            this.expectedValue = testPhoto
            populateInput(testPhoto)
            propagate()


            val bestResult = this.layers.last().layerOutputs()

            val expected = this.layers.last().layerOutputs()
            if (bestResult == expected) {
                success++
            }
        }
        saveMeasures()
        println("Success rate: ${success.toDouble() / this.learningSet.size}")
    }

    private fun saveMeasures() {
        File("errors.csv").printWriter().use { out ->
            this.errors.forEachIndexed { i, error ->
                out.println("$error")
            }
        }
    }
}