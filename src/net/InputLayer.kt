package net

/**
 * Created by rebzden on 16/11/16.
 */
class InputLayer(var size: Int) : Layer(size, 0) {

    override fun toString(): String {
        return "net.InputLayer(neuronCount=$neuronCount, inputs=${this.layerInputs().size} outputs=${this.layerOutputs().size})"
    }
}