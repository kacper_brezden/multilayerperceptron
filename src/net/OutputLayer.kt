package net

/**
 * Created by rebzden on 05/11/16.
 */
class OutputLayer(var size: Int, var weightsAmount: Int) : Layer(size, weightsAmount) {

    override fun toString(): String {
        return "net.OutputLayer(neuronCount=$neuronCount, inputs=${this.layerInputs().size} outputs=${this.layerOutputs()})"
    }
}