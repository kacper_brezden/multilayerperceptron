package net

/**
 * Created by rebzden on 17/11/16.
 */
interface ActivationFunction {

    fun evaluate(value: Double): Double
    fun evaluateDerivate(value: Double): Double
}