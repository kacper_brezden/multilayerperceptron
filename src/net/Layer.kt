package net

import java.util.*

/**
 * Created by rebzden on 05/11/16.
 */
abstract class Layer(var neuronCount: Int, var weightsCount: Int) {
    var neurons: ArrayList<Neuron> = arrayListOf()

    init {
        createNeurons()
        randomizeWeights()
    }

    fun randomizeWeights() {
        var neuronWeights: ArrayList<Double>
        for (neuron in this.neurons) {
            neuronWeights = arrayListOf()
            for (j in 1..this.weightsCount) {
                neuronWeights.add(generateRandom())
            }
            neuron.bias = generateRandom()
            neuron.weights = neuronWeights
        }
    }

    fun generateRandom(): Double {
        var rand = Random()
        return -0.5 + (0.5 + 0.5) * rand.nextDouble()
    }

    fun createNeurons() {
        for (i in 1..this.neuronCount) {
            neurons.add(Neuron())
        }
    }

    fun layerOutputs(): ArrayList<Double> {
        var outputs: ArrayList<Double> = arrayListOf()
        this.neurons.mapTo(outputs) { it.output }
        return outputs
    }

    fun layerInputs(): ArrayList<Double> {
        var inputs: ArrayList<Double> = arrayListOf()
        if (this.neurons.size > 0) {
            inputs = this.neurons[0].inputs
        }
        return inputs
    }

    fun layerDeltas(): ArrayList<Double> {
        var deltas: ArrayList<Double> = arrayListOf()
        this.neurons.mapTo(deltas) { it.delta }
        return deltas
    }

    fun updateWeights(learningRate: Double) {
        for (neuron in this.neurons) {
            var newWeights: ArrayList<Double> = arrayListOf()
            println(neuron.weights.sum())
            for (weight in neuron.weights) {
                var newWeight = weight + (learningRate * neuron.delta * neuron.output)
            }
        }
    }

    fun countError(): Double {
        var error = 0.0
        this.neurons.forEach { neuron ->
            error += neuron.countError()
        }
        return error
    }
}