package fx

/**
 * Created by Rebzden on 16.12.2016.
 */

import javafx.application.Application
import javafx.stage.Stage
import tornadofx.App

class Backpropagation : App(AutoencoderView::class) {

    val autoencoderController: AutoencoderController by inject()

    override fun start(stage: Stage) {
        super.start(stage)
        autoencoderController.init()
    }
}
fun main(args: Array<String>) {
    Application.launch(Backpropagation::class.java, *args)
}
