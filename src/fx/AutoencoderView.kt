package fx

import javafx.scene.Group
import javafx.scene.canvas.Canvas
import javafx.scene.chart.*
import javafx.collections.FXCollections
import javafx.scene.layout.FlowPane
import javafx.scene.layout.HBox
import tornadofx.*

/**
 * Created by Rebzden on 16.12.2016.
 */
class AutoencoderView : View() {
    override val root = Group()
    var errorChart: XYChart<Number, Number> by singleAssign()
    var layout: FlowPane by singleAssign()

    init {
        title = "Autoencoder"

        with(root) {
            vbox {
                layout = flowpane {

                }
                layout.prefWrapLength = 624.0
                layout.vgap = 8.0
                layout.hgap = 4.0
            }
        }
    }
}


