package fx

import javafx.application.Platform
import javafx.scene.canvas.Canvas
import javafx.scene.chart.XYChart
import javafx.scene.image.ImageView
import javafx.scene.paint.Color
import net.NeuralNet
import net.SigmoidFunction
import tornadofx.Controller
import tornadofx.FX
import java.util.*

/**
 * Created by rebzden on 16/12/16.
 */
class AutoencoderController : Controller() {
    val autoencoderView: AutoencoderView by inject()

    fun init() {
        with(config) {
            FX.primaryStage.scene.root = autoencoderView.root
            FX.primaryStage.sizeToScene()
            FX.primaryStage.centerOnScreen()
            prepareData()
//            autoencoderView.drawShapes()
        }
    }

    fun prepareData() {
        var startTime = System.nanoTime()
        var network = NeuralNet("dataset", "testset", 10, 1,70, 0.5, SigmoidFunction(), 50, 2000, 0.0, 0.0)
        network.check()
        var aValue = 1.56
        val aSeries = XYChart.Series<Number, Number>()
        aSeries.name = "error"
        for (i in 0..network.errors.size - 1) {
            aValue = network.errors[i]
            aSeries.data.add(XYChart.Data(i, aValue))
        }

        Platform.runLater {
//            autoencoderView.errorChart.data.add(aSeries)
            network.layers[1].neurons.forEach { neuron ->
                val can = Canvas(network.inputWidth * 10, network.inputHeight * 10)
                autoencoderView.layout.children.add(can)
                val gc = can.graphicsContext2D
                for (i in 0..network.inputWidth.toInt() - 1) {
                    for (j in 0..network.inputHeight.toInt() - 1) {
                        var weightedPixel = (Math.abs(neuron.weights[i * network.inputHeight.toInt() + j]) * 255).toInt()
                        if (weightedPixel > 255) {
                            weightedPixel = 255
                        }
                        gc.fill = Color.rgb(weightedPixel, weightedPixel, weightedPixel)
                        gc.lineWidth = 5.0
                        gc.fillRect(i.toDouble() * 10, j.toDouble() * 10, 100.0, 100.0)
                    }
                }
            }
            val output = network.layers.last().layerOutputs()
            val can = Canvas(network.inputWidth * 10, network.inputHeight * 10)
            autoencoderView.layout.children.add(can)
            val gc = can.graphicsContext2D
            for (i in 0..network.inputWidth.toInt() - 1) {
                for (j in 0..network.inputHeight.toInt() - 1) {
                    var weightedPixel = 0
                    weightedPixel = (Math.abs(output[i * network.inputHeight.toInt() + j]) * 255).toInt()
                    println(weightedPixel)

                    if (weightedPixel > 255) {
                        weightedPixel = 255
                    }
                    gc.fill = Color.rgb(weightedPixel, weightedPixel, weightedPixel)
                    gc.lineWidth = 5.0
                    gc.fillRect(i.toDouble() * 10, j.toDouble() * 10, 100.0, 100.0)
                }
            }


        }
        var endTime = System.nanoTime()
        println("Time ${endTime - startTime}")
    }

}